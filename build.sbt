enablePlugins(ScalaJSPlugin)

name := "Pacifier"
scalaVersion := "2.13.0"

libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.7"

skip in packageJSDependencies := false
jsDependencies += "org.webjars" % "jquery" % "3.4.1" / "jquery.js" minified "jquery.min.js"

jsEnv := new org.scalajs.jsenv.jsdomnodejs.JSDOMNodeJSEnv()

// uTest settings
libraryDependencies += "com.lihaoyi" %%% "utest" % "0.7.1" % "test"
libraryDependencies += "com.lihaoyi" %%% "upickle" % "0.8.0"
libraryDependencies += "io.udash" %%% "udash-jquery" % "3.0.2"

testFrameworks += new TestFramework("utest.runner.Framework")
