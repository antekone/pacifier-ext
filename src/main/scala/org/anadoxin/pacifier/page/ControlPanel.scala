package org.anadoxin.pacifier.page

import org.anadoxin.pacifier.browser.ResourceLoader
import org.anadoxin.pacifier.engines.CommentData
import org.anadoxin.pacifier.log.Log
import org.scalajs.dom
import org.scalajs.dom.raw.HTMLElement

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}
import io.udash.wrappers._
import io.udash.wrappers.jquery.jQ
import org.anadoxin.pacifier.PacifierEntryPoints

object ControlPanel {
  import ExecutionContext.Implicits.global

  def show(cdJson: String): Unit = {
    val cd = upickle.default.read[CommentData](cdJson)
    ResourceLoader.getURL(cd.extURL, "comment_hook.html").onComplete {
      case Success(res) => ()
      case Failure(err) =>
        // Alert?
        Log.put(s"Can't load comment_hook resource: ${err.getMessage}")
    }
  }
}
