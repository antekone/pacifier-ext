package org.anadoxin.pacifier

import org.anadoxin.pacifier.contentscript.Page
import org.anadoxin.pacifier.log.Log
import org.anadoxin.pacifier.page.ControlPanel
import org.anadoxin.pacifier.background._

import scala.scalajs.js.annotation._

@JSExportTopLevel("Pacifier")
object PacifierEntryPoints {
  // Executed from Background Script context
  @JSExport
  def runBackgroundService(): Unit = BackgroundService().run()

  // Executed from Content Script context
  @JSExport
  def runPageHandler(): Unit = Page.instance().run()

  // Executed from Page context
  @JSExport
  def runCommentHook(cdJson: String): Unit = ControlPanel.show(cdJson)

  @JSExport
  def hideAllPanels(): Unit = {
    Log.put("hide all panels")
  }
}
