package org.anadoxin.pacifier.browser

import scala.scalajs.js
import scala.scalajs.js.annotation._

package object ext {
  lazy val browser: Browser = js.Dynamic.global.browser.asInstanceOf[Browser]
}

@js.native
trait BrowserRuntime extends js.Object {
  def getURL(path: String): String = js.native
}

@js.native
trait Browser extends js.Object {
  val runtime: BrowserRuntime = js.native
}

@js.native
@JSGlobal
object BrowserGlobalScope extends js.Object {
  val browser: Browser = js.native
}
