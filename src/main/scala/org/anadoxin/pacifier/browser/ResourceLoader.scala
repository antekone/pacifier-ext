package org.anadoxin.pacifier.browser

import scala.concurrent.{ExecutionContext, Future, Promise}
import org.scalajs.dom.ext.Ajax

import scala.util.{Failure, Success}

object ResourceLoader {
  import ExecutionContext.Implicits.global

  // Allowed only in Content Script context
  def get(filename: String): Future[String] = {
    val url = ext.browser.runtime.getURL(filename)
    this.getURL(url)
  }

  // Allowed in all contexts
  def getURL(url: String, file: String): Future[String] = {
    val sep = if(url.endsWith("/")) { "" } else { "/" }
    this.getURL(s"$url$sep$file")
  }

  def getURL(url: String): Future[String] = {
    val returnPromise = Promise[String]()
    if(url == null || url.isEmpty) {
      return returnPromise.failure(
        new RuntimeException(
          s"ResourceLoader: failed to get web resource " +
          s"url for '$url'")).future
    }

    Ajax.get(url).onComplete {
        case Success(req) =>
          returnPromise.success(req.response.toString)

        case Failure(err) =>
          returnPromise.failure(
            new RuntimeException(
              s"ResourceLoader: failed to issue async request for " +
              s"'$url': ${err.getMessage}")
          )
      }

      returnPromise.future
    }
}
