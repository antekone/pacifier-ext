package org.anadoxin.pacifier.engines

import org.scalajs.dom

case class Entity(author: String, element: dom.Element) {
  val uniqueId = UniqueGen.next()
  def id = this.uniqueId

  override def toString(): String = {
    return s"Entity[id=$id,author=$author]"
  }
}

//object Entity {
  //def apply(author: String, element: dom.Element): Entity = {
    //new Entity(author, element)
  //}
//}
