package org.anadoxin.pacifier.engines

import org.scalajs.dom
import org.anadoxin.pacifier.log.Log
import scala.collection.{Seq, Map}

class WykopEngine extends Engine {
  import org.scalajs.dom.ext._

  override def run(doc: dom.Document): Unit = {
    Log.put(s"--- Running on ${dom.window.location.href}")
    val comments = doc.getElementsByTagName("div")
      .filter(_.attributes.getNamedItem("data-type") != null)
      .filter(_.attributes.getNamedItem("data-type").nodeValue == "comment")

    comments.foreach(div => {
      val relevantLinks = div.querySelectorAll("a").filter(a => {
        a.attributes.getNamedItem("href") != null && a.attributes.getNamedItem("href").nodeValue.contains("/ludzie/")
      })

      Log.put(s"Comment by ${relevantLinks}")
    })

    Log.put(s"Found ${comments.size} comments.")
  }
}

object WykopEngine {
  def apply(): Engine = new WykopEngine()
}
