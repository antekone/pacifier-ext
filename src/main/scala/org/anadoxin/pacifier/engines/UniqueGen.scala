package org.anadoxin.pacifier.engines

object UniqueGen {
  var currentUniqueNumber: Int = 0

  def next(): Int = {
    this.currentUniqueNumber += 1
    this.currentUniqueNumber
  }
}
