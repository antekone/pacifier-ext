package org.anadoxin.pacifier.engines
import org.scalajs.dom
import upickle.default._

import scala.language.implicitConversions

trait Engine {
  def run(doc: dom.Document): Unit

  implicit def stringToRuntimeException(s: String): RuntimeException = {
    new RuntimeException(s)
  }
}

case class CommentData(id: Int, extURL: String)
object CommentData {
  implicit def rw: ReadWriter[CommentData] = macroRW
}
