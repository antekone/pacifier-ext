package org.anadoxin.pacifier.engines

import org.anadoxin.pacifier.browser.ext

import scala.util.{Failure, Success, Try}
import org.scalajs.dom
import org.anadoxin.pacifier.log.Log

import scala.collection.{Map, Seq, mutable}
import scala.scalajs.js.JSON

class LobstersEngine extends Engine {
  import org.scalajs.dom.ext._

  var commentMap: mutable.Map[Int, Entity] = mutable.Map()

  override def run(doc: dom.Document): Unit = {
    val elems = doc.getElementsByTagName("div")
      .filter((node) => {
        node.hasAttribute("id") && node.hasAttribute("class")
      }).filter((node) => {
        node.getAttribute("id").startsWith("c_")
      });

    Log.put(s"lobste.rs: found ${elems.size} comments on this page")
    if(elems.nonEmpty) {
      this.processComments(elems)
    }
  }

  def processComments(elems: Seq[dom.Element]): Unit = {
    this.processCommentsWithResources(elems)
  }

  def processCommentsWithResources(elems: Seq[dom.Element]): Unit = {
    elems.foreach(e => {
      e.querySelectorAll("div.byline > a").filter (a => {
        a.attributes.getNamedItem("name") == null && a.attributes.getNamedItem("href") != null
      }).filter(a => {
        !a.childNodes.exists(child => {
          child.nodeName == "img"
        })
      }).filter(a => {
        a.textContent.trim() != "link" &&
          a.attributes("href") != null &&
          a.attributes("href").value.contains("/u/")
      }).foreach(a => {
        val author = a.textContent.trim()
        if(!author.isEmpty) {
          val entity = Entity(author, e)
          this.commentMap += (entity.id -> entity)
          this.injectControlPanel(entity)
        }
      })
    })
  }

  def injectControlPanel(e: Entity): Try[Unit] = {
    val bylineEl = e.element.querySelector("div > div.details > div.byline")
    if(bylineEl == null)
      return Failure("Injection point find error #1")

    val textNode = dom.document.createTextNode(" | ")
    val panelLinkNode = dom.document.createElement("a")

    val id = e.uniqueId
    val extUrl = ext.browser.runtime.getURL("")
    val jsonArg = JSON.stringify(upickle.default.write(CommentData(id, extUrl)))

    panelLinkNode.id = s"element-$id"
    panelLinkNode.setAttribute("onClick", s"Pacifier.runCommentHook($jsonArg)")
    panelLinkNode.innerHTML = "Pacifier"

    bylineEl.appendChild(textNode)
    bylineEl.appendChild(panelLinkNode)

    Success()
  }
}

object LobstersEngine {
  def apply(): Engine = new LobstersEngine()
}
