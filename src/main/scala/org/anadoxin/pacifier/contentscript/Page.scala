package org.anadoxin.pacifier.contentscript

import org.scalajs.dom
import org.anadoxin.pacifier.engines._
import scala.collection._

class Page {
  def run(): Unit = {
    Page.engines.get(dom.window.location.hostname) match {
      case Some(engine) =>
        engine.run(dom.document)
      case None => ()
    }
  }
}

object Page {
  val engines: immutable.Map[String, Engine] = immutable.Map(
    "lobste.rs" -> LobstersEngine(),
    "www.wykop.pl" -> WykopEngine(),
  )

  var inst: Page = _
  def instance(): Page = {
    if(this.inst == null) {
      this.inst = new Page();
    }

    this.inst
  }
}
