package org.anadoxin.pacifier.log

object Log {
  def put(str: String): Unit = {
    println(s"[Pcfr-LOG] $str")
  }
}
