package org.anadoxin.pacifier.background

import org.scalajs.dom
import org.anadoxin.pacifier.browser.ext
import org.anadoxin.pacifier.log.Log

class BackgroundService {
    def openPage(): Unit = {
        Log.put("Opened page")
    }

    def run(): Unit = {
    }
}

object BackgroundService {
    var instance: Option[BackgroundService] = None
    def apply(): BackgroundService = {
        this.instance match {
            case Some(i) => i
            case None => 
                this.instance = Some(new BackgroundService())
                this.instance.get
        }
    }
}