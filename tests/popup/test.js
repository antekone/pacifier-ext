var mx = 0;
var my = 0;

window.onmousemove = function(ev) {
    mx = ev.clientX;
    my = ev.clientY;
}

function showPopup() {
    var el = $("#cp");
    $("#dim-screen").fadeIn("fast");
    el.css("left", mx - 24);
    el.css("top", my - 24);
    el.fadeIn("fast");
}

function hidePopup() {
    $("#cp").fadeOut("fast");
    $("#dim-screen").fadeOut();
}
