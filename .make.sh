#!/usr/bin/env bash

#./.lint.sh
#if [ ! "$?" == "0" ]; then
    #echo "Lint failed, press ENTER."
    #exit 1
#fi

if [ "$1" == "--release" ]; then
    method="fullOptJS"
    suffix="opt"
    jsdeps="jsdeps.min"
else
    method="fastOptJS"
    suffix="fastopt"
    jsdeps="jsdeps"
fi

echo Reloading...
sbt-client reload | sed -ur "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g"
if [ ! "${PIPESTATUS[0]}" == "0" ]; then
    echo "Reload failed, press ENTER."
    exit 1
fi

echo Building...
sbt-client $method | sed -ur "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g"
if [ ! "${PIPESTATUS[0]}" == "0" ]; then
    echo "Build failed, press ENTER."
    exit 1
fi

ls -la target/scala-2.13/pacifier-$suffix.js
ls -la target/scala-2.13/pacifier-$jsdeps.js

cp -v target/scala-2.13/pacifier-$suffix.js ext/pacifier.js
cp -v target/scala-2.13/pacifier-$jsdeps.js ext/pacifier-jsdeps.js
cp -v target/scala-2.13/pacifier-$suffix.js.map ext/pacifier-$suffix.js.map

echo "Build OK"
exit 0
